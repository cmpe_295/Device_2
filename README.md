CMPE 295 Device 1

Getting Started

This program is part of the use case and simulates the vehicle that receives
a warning when the other vehicle has a flat tire.

Prerequisites

The client must have Git installed to clone the repo.

Installing

Clone the repo then run the following command:

sudo python Device_2.py

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
